import random

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Page

bootsrap_page = """
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Best web</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Home</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/table">tabla</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.youtube.com/watch?v=isioQPUiAVk">Mondongo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.youtube.com/watch?v=Q36_g3fScJY">Como aprobar ASS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.youtube.com/watch?v=hk2dxfDtGeM">Despues de aprobar ASS</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="/home/alumnos/jose/PycharmProjects/django-cms-bootstrap/pages/imagen1.jpg" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>First slide label</h5>
            <p>Some representative placeholder content for the first slide.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="/home/alumnos/jose/PycharmProjects/django-cms-bootstrap/pages/imagen2.jpg" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>Second slide label</h5>
            <p>Some representative placeholder content for the second slide.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="/home/alumnos/jose/PycharmProjects/django-cms-bootstrap/pages/imagen3.jpg" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>Third slide label</h5>
            <p>Some representative placeholder content for the third slide.</p>
          </div>
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
    <div>
        {body}
    </div>
    <!-- Enlace al script de Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  </body>
</html>
"""
boodstrap_table = """<table class="table">
  <thead>
    <tr>
      <th scope="col">key</th>
      <th scope="col">content</th>
      <th scope="col">modify</th>
    </tr>
  </thead>
  <tbody>
    {table}
  </tbody>
</table>
    </div>"""

boodstrap_grid = """<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>{nombre}</h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>{contenido}</h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h3>Solo has un put!</h3>
                </div>
            </div>
        </div>
    </div>
</div>

"""



boodstrap_navbar = """
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Home</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">/table</a>
                    </li>
                    <!-- Agrega más enlaces aquí según sea necesario -->
                </ul>
            </div>
        </div>
    </nav>
"""

html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <link href="main.css" rel="stylesheet">
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"

html_content_template = "<p> {content} </p>"

html_table_template = """<tr>
      <td>{key}</td>
      <td>{content}</td>
      <td>Just make a put!</td>
    </tr>"""






def index(request):
    pages = Page.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return HttpResponse(bootsrap_page.format(body=body))


@csrf_exempt
def page(request, name):
    if request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
        except Page.DoesNotExist:
            p = Page(name=name)
        p.content = request.body.decode("utf-8")
        p.save()

    if request.method == 'GET' or request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
            content = p.content
            contentToPage = boodstrap_grid.format(nombre=name, contenido=content)
            response = (HttpResponse(bootsrap_page.format(body=contentToPage)))
        except Page.DoesNotExist:
            content = "Page " + name + " not found"
            response = (HttpResponse(html_template.format(body=content)))
        return response


def style(request):
    colores_html = ['yellow', 'blue', 'white', 'gold', 'gray', 'brown',
                    'purple', 'orange', 'black', 'silver', 'red', 'pink',
                    'green']
    color_color = random.choice(colores_html)
    color_background = random.choice(colores_html)

    content = """
    body {
margin: 10px 20% 50px 70px;
font-family: sans-serif;
color: """ + color_color + """;
background: """ + color_background + """;
}"""
    # esto es para que el tipo de texto se css y no html
    content_type = 'text/css'
    return HttpResponse(content, content_type=content_type)


def table(request):
    to_table = ""
    for i in Page.objects.all():
        key = i.name
        content = i.content
        to_table += html_table_template.format(key=key, content=content)
    table_format = boodstrap_table.format(table=to_table)
    response = (HttpResponse(bootsrap_page.format(body=table_format)))
    return response
